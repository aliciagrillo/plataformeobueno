﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoEnemigo : MonoBehaviour
{
    public GameObject milaser;
    public AudioSource disparo;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Dispara());

    }

    IEnumerator Dispara()
    {
        GameObject go;
        while (true)
        {
            go = Instantiate(milaser);
            disparo.Play();
            go.transform.position = transform.position;

            yield return new WaitForSeconds(1.0f);
        }
    }
}
