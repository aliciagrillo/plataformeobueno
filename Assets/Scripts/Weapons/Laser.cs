﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public float speed;
    public Vector2 direccion;
    void Update()
    {
        transform.Translate(direccion * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish" || other.tag == "Patrullero"|| other.tag == "Suelo" || other.tag == "Boss")
        {
            Destroy(gameObject);
        }
    }

}
