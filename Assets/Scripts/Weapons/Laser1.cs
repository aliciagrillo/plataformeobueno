﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser1 : MonoBehaviour
{
    public float speed;
    public Vector2 direccion;
    void Update()
    {
        transform.Translate(direccion * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish" || other.tag == "Suelo" || other.tag == "Player")
        {
            Destroy(gameObject);
        }
    }

}
