﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRanger : MonoBehaviour
{
    [SerializeField] float rangeDistanceMin;
    [SerializeField] float rangeDistanceMax;
    float rangeDistance = 6;
    [SerializeField] Transform player;
    [SerializeField] AudioSource audioSource;
    [SerializeField] float velocidadEnemigo;
    [SerializeField] MovPlataforma patrol;
    Color colorgizmo = Color.yellow;
    public GameObject milaser;
    public AudioSource disparo;
    private float timeCounter;
    private float timeToShoot;
    private bool isShooting;
    private float timeShooting;
    private float speedx;

    public Animator animator;

    private void Awake()
    {
        rangeDistance = rangeDistanceMin;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    protected virtual void Inicitialization()
    {
        timeCounter = 0.0f;
        timeToShoot = 3.0f;
        timeShooting = 1.0f;
        isShooting = false;
    }
    protected virtual void EnemyBehaviour()
    {
        timeCounter += Time.deltaTime;
        //Sistema de tiro que no va
        /*  if (timeCounter > timeToShoot)
          {
              if (!isShooting)
              {
                  isShooting = true;
                  Instantiate(milaser, this.transform.position, Quaternion.Euler(0, 0, 180), null);

              }
              if (timeCounter > (timeToShoot + timeShooting))
              {
                  timeCounter = 0.0f;
                  isShooting = false;
              }
          }*/

    }
    private void Update()
    {
        EnemyBehaviour();
        if (Mathf.Abs(Vector2.Distance(player.position, transform.position)) < rangeDistance)
        {
            colorgizmo = Color.red;
            velocidadEnemigo = 3;
            rangeDistance = rangeDistanceMax;
            patrol.enabled = false;
            transform.position = Vector2.MoveTowards(transform.position, player.position, Time.deltaTime * velocidadEnemigo);
            animator.SetBool("Tehevisto", true);
        }
        else
        {
            colorgizmo = Color.yellow;
            rangeDistance = rangeDistanceMin;
            patrol.enabled = true;
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Trampa")
        {

            StartCoroutine(Muerto());

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            StartCoroutine(Muerto());
        }

    }
    IEnumerator Muerto()
    {
        audioSource.Play();
        yield return new WaitForSeconds(0.2f);
        Destroy(this.gameObject);
        animator.SetBool("IsDead", true);
    }
   
    
    private void OnDrawGizmos()
    {
        Gizmos.color = colorgizmo;
        Gizmos.DrawWireSphere(transform.position, rangeDistance);
    }
}


