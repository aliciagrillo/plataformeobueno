﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] float rangeDistanceMin;
    [SerializeField] float rangeDistanceMax;
    float rangeDistance = 6;
    [SerializeField] Transform player;
    [SerializeField] float velocidadEnemigo;
    [SerializeField] MovPlataforma patrol;
    Color colorgizmo = Color.yellow;
    [SerializeField] AudioSource audioSource;
    [SerializeField] public Animator animator;


    private void Awake()
    {
        rangeDistance = rangeDistanceMin;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //animator.SetBool("Tehevisto", false);
        //animator = GetComponent<animator>();
    }


    private void Update()
    {
        if (Mathf.Abs(Vector2.Distance(player.position, transform.position)) < rangeDistance)
        {
            colorgizmo = Color.red;
            rangeDistance = rangeDistanceMax;
            patrol.enabled = false;
            transform.position = Vector2.MoveTowards(transform.position, player.position, Time.deltaTime * velocidadEnemigo);
            animator.SetBool("Tehevisto", true);
        }
        else
        {
            colorgizmo = Color.yellow;
            rangeDistance = rangeDistanceMin;
            patrol.enabled = true;
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Trampa")
        {
            
            StartCoroutine(Muerto());

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            StartCoroutine(Muerto());
        }

    }
    IEnumerator Muerto()
    {
        audioSource.Play();
        yield return new WaitForSeconds(0.2f);
        Destroy(this.gameObject);
        animator.SetBool("IsDead", true);

    }
    private void OnDrawGizmos()
    {
        Gizmos.color = colorgizmo;
        Gizmos.DrawWireSphere(transform.position, rangeDistance);
    }
}
