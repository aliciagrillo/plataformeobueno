﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    [SerializeField] GameObject[] sprites;
    private int elegido;

    [SerializeField] BoxCollider2D collider;
   // [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioSource disparo;
    private float timeCounter;
    public float timeToShoot;
    public Vector2 sitio;
    private float timeShooting;



    private float speedx;
    public Vector2 limits;

    private bool isShooting;
    public int fase ;
    public int vida = 300;
    public int angulo;
    public float speed;
    private bool direccion = true;


    [SerializeField] GameObject bullet;

    private void Awake()
    {

        elegido = Random.Range(0, sprites.Length);

        for (int kk = 0; kk < sprites.Length; kk++)
        {
            sprites[kk].SetActive(false);
        }

        sprites[elegido].SetActive(true);


        Inicitialization();
    }

    protected virtual void Inicitialization()
    {
        timeCounter = 0.0f;
        speedx = 0.0f;
        timeToShoot = 1.0f;
        isShooting = false;
        timeShooting = 1.0f;
    }

    protected virtual void EnemyBehaviour()
    {
       
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        EnemyBehaviour();
       
        if (vida <= 300)
        {
            fase = 1;
        }
        else if (vida <= 200)
        {
            fase = 2;
        }
        else if (vida <= 100)
        {
            fase = 3;
        }


        if (vida == 0)
        {
            StartCoroutine(DestroyShip());
        }
        timeCounter += Time.deltaTime;
        if (fase == 1)
        {
            /* transform.position(sitio * speed * Time.deltaTime);
             if (transform.position.x > limits.x)
             {
                 transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
                 StartCoroutine(DestroyShip());
             }*/
            if (timeCounter > timeToShoot)
            {
                if (!isShooting)
                {
                    isShooting = true;
                    Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
                    disparo.Play();

                }
                if (timeCounter > (timeToShoot + timeShooting))
                {
                    timeCounter = 0.0f;
                    isShooting = false;
                }
            }
        }
        if (fase == 2)
        {
            if (timeCounter > timeToShoot)
            {
                if (!isShooting)
                {
                    isShooting = true;
                    Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 240), null);
                    Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
                    Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 120), null);
                    disparo.Play();

                }
                if (timeCounter > (timeToShoot + timeShooting))
                {
                    timeCounter = 0.0f;
                    isShooting = false;
                }
            }
        }

        if (fase == 3)
        {
            if (timeCounter > timeToShoot)
            {

                Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, angulo + 90), null);
                disparo.Play();
                if (angulo > 180)
                {
                    direccion = false;
                }
                if (angulo < 0)
                {
                    direccion = true;
                }
                if (direccion == true)
                {
                    angulo += 10;
                }
                else
                {
                    angulo -= 10;
                }
                timeCounter = 0;
            }
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            //StartCoroutine(DestroyShip());
            vida = vida - 5;
        }


    }

    IEnumerator DestroyShip()
    {

        //Desactivo el grafico
        sprites[elegido].SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        //ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("GameOver");

        Destroy(this.gameObject);
    }
}
