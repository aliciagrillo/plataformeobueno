﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    public void PulsaPlay()
    {
        //Debug.LogError("He pulsado Play");

        SceneManager.LoadScene("GAME");
    }

    public void PulsaExit()
    {
        Application.Quit();
    }
}
