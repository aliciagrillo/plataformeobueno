﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]

public class Personaje : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    private bool jump;
    public Weapon weapon;
    public float shootTime = 0f;
    //public AudioSource audioSource;
    public float fuerzaSalto = 5.0f;
    public bool isGrounded = false;
    public GameObject graphics;
    //[SerializeField] private Animator animator;
    private float escalactual;
    private bool isDead = false;
    public AudioSource Muerte;
    public AudioSource portal;
    public Animator animator;


    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        //rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

         if (Math.Abs(rb2d.velocity.x) > 0.01f)
         {
             animator.SetFloat("Velocidad", Math.Abs(rb2d.velocity.x));
         }

        if (isDead) { return; }
    }

    private void Update()
    {
        shootTime += Time.deltaTime;
        move = Input.GetAxis("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);
        if (isDead) { return; }

        if (jump && isGrounded)
        {
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
            
            StartCoroutine(salto());
        }

        escalactual = graphics.transform.localScale.x;

        if (move > 0 && escalactual < 0)
        {
            graphics.transform.localScale = new Vector3(1, 1, 1);
        }
        else if (move < 0 && escalactual > 0)
        {
            graphics.transform.localScale = new Vector3(-1, 1, 1);
        }

        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrounded = true;
        if (collision.gameObject.tag == "Portal")
        {
            portal.Play();
            SceneManager.LoadScene("Cristian");
        }
        if (collision.gameObject.tag == "EnemyBullet")
        {
            StartCoroutine(Muerto());
            //SceneManager.LoadScene("GameOver");
        }
        else if (collision.gameObject.tag == "EnemyBullet")
        {
            //SceneManager.LoadScene("GameOver");
            StartCoroutine(Muerto());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
         if (collision.gameObject.tag == "PlatMov")
        {
            //rb2d.velocity = new Vector2(0, 0);
            this.transform.parent = collision.transform;
        }
        else if (collision.gameObject.tag == "Trampa")
        {
            //SceneManager.LoadScene("GameOver");
            StartCoroutine(Muerto());
        }
        else if (collision.gameObject.tag == "EnemyBullet")
        {
            //SceneManager.LoadScene("GameOver");
            StartCoroutine(Muerto());
        }
        else if (collision.gameObject.tag == "Patrullero")
        {
            //SceneManager.LoadScene("GameOver");
            StartCoroutine(Muerto());
        }
        else if (collision.gameObject.tag == "Ranger")
        {
            //SceneManager.LoadScene("GameOver");
            StartCoroutine(Muerto());
        }
        /*  else if (collision.gameObject.tag == "Trampolin")
          {
              rb2d.AddForce(Vector2.right * move * maxS * 4);
          }*/
    }

    public void Shoot()
    {

        if (!isDead && shootTime > weapon.GetCadencia())
        {
            shootTime = 0;
            weapon.Shoot();
        }
    }

    void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.tag == "PlatMov")
            {
                this.transform.parent = null;
            }
        }
     IEnumerator Muerto(){
        isDead = true;
        Muerte.Play();
        animator.SetBool("Rip", true);

        yield return new WaitForSeconds(1.0f);
         SceneManager.LoadScene("GameOver");

         }
    IEnumerator salto()
    {
        animator.SetBool("Jump", true);
        yield return new WaitForSeconds(1.0f);
        animator.SetBool("Jump", false);

    }
}
